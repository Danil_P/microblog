import wtforms
from wtforms import validators
from wtforms.validators import DataRequired
from app.models import Post 

class PostForm(wtforms.Form):
      body = wtforms.TextAreaField("", validators=[validators.DataRequired()]);