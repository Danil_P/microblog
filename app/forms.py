import wtforms
from wtforms import validators
from wtforms.validators import DataRequired
from app.models import User 


class RegisterForm(wtforms.Form):
      email = wtforms.StringField("Email", validators = [validators.DataRequired()], default="input");
      name = wtforms.StringField("Name", validators = [validators.DataRequired()]);
      password = wtforms.StringField("Password", validators = [validators.DataRequired()]);

      def validate(self):
            if not super(RegisterForm, self).validate():
                  return False;
            if User.query.filter(User.email == self.email.data).first():
                  self.email.errors.append("Try another email");
                  return False;
            return True;

class ImageForm(wtforms.Form):
      file = wtforms.FileField('Image file');

class LoginForm(wtforms.Form):
      email = wtforms.StringField("Email", validators=[validators.DataRequired()]);
      password = wtforms.PasswordField("Password", validators=[validators.DataRequired()]);

      def validate(self):
            if not super(LoginForm, self).validate():
                  return False;
            self.user = User.authenticate(self.email.data, self.password.data);
            if not self.user:
                  self.email.errors.append("Invalid password or email");
                  return False;
            return True;