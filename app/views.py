import os;
from flask import redirect, render_template, url_for, request, flash;
from flask_login import login_user, logout_user
from app import app, db, login_manager
from app.models import User, Post;
from app.forms import RegisterForm, ImageForm, LoginForm;
from werkzeug import secure_filename
from app import g

@app.route('/')
def index():
      if g.user.is_authenticated:
            return redirect( url_for('users.profile', id = g.user.id) );
      

      return render_template(
            "index.html",
      );

@app.route('/register', methods=['GET', 'POST'])
def register():
      if g.user.is_authenticated:
            return redirect( url_for('index') );
      if request.method == 'POST':
            form = RegisterForm(request.form);
            if form.validate():
                  user = User();
                  form.populate_obj(user);
                  user.password_hash = User.make_password(form.password.data);
                  db.session.add(user);
                  db.session.commit();
                  login_user(user);
                  return redirect( url_for('upload_avatar', id = user.id) );
      else:
            form = RegisterForm();
      if not form:
            form = RegisterForm();
      return render_template(
            "register.html",
            form = form
      );

@app.route('/upload-avatar/<int:id>', methods=['GET', 'POST'])
def upload_avatar(id):
      import random;
      user = User.query.get_or_404(id);

      if g.user != user:
            return redirect( url_for('index'));

      form = ImageForm();
      if not user:
            flash("uncorrect user", 'success');
            return redirect( url_for('index') );
      if request.method == 'POST':
            image_file = request.files['file'];
            k = str(random.randint(1, 23))
            filename = os.path.join(app.config['IMAGES_DIR'],
                  ( k + secure_filename(image_file.filename)));
            image_file.save(filename);
            user.photo = "http://localhost:5000/static/images/" +  k + image_file.filename;
            db.session.add(user);
            db.session.commit();
            return redirect( url_for('index') );
      else :
            return render_template('upload_image.html', 
                  user = user,
                  form = form
            );

@app.route('/login', methods = ['GET', 'POST'])
def login():

      if g.user.is_authenticated:
            return redirect( url_for('index') );

      if request.method == 'POST':
            form = LoginForm(request.form);
            if form.validate():
                  login_user(form.user);
                  flash("Successfully logged in as %s." % form.user.email, "success")
                  return redirect(url_for('index'));
      else:
            form = LoginForm();
      return render_template('login.html', form = form )

@app.route('/logout')
def logout():
      logout_user();
      return redirect(url_for('index'));


@app.route('/all_users')
def all_users():
      users = User.query.all();
      return render_template('all_users.html', users = users);
